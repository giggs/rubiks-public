# Rubiks Simulator


## Description

This is an interactive Rubiks Simulator coded in C using raylib, made as my first project after learning the language.


## Features

- Mouse controls
- Player controlled orbital camera
- Undo/Redo buffer
- Two types of cube, one standard and one with custom textures
- Timer
- Move counter

## Controls

- Hold left click and move mouse to rotate a side 
- Hold right click and move mouse to rotate the camera    
- Scroll down or Backspace to undo  
- Scroll up or Enter to redo  
- Press S to scramble the cube (restarts timer and move counter) 
- Press O to bring the camera back to its starting point  
- Press ESC to go back to the menu (progress will be lost)  
- If the cube rotates on itself when you're trying to rotate around it, press F

## Links

- [Making of](https://giggs.gitlab.io/blog/rubiks-cube/)
- [raylib](https://www.raylib.com/index.html)
- [Quick Windows setup](https://github.com/raysan5/raylib/wiki/Working-on-Windows)
- [Webapp version](https://giggs.gitlab.io/rubiks/app/cube.html)
- [Compiling to WebAssembly](https://github.com/raysan5/raylib/wiki/Working-for-Web-(HTML5))

## License

This game sources are licensed under an unmodified zlib/libpng license, which is an OSI-certified, BSD-like license that allows static linking with closed source software.
