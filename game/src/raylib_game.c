/*******************************************************************************************
*
*   This game has been created using raylib (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2022 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
#if defined(PLATFORM_WEB)
#include <emscripten.h>
#endif
#include "raylib.h"
#include "rlgl.h"
#include "raymath.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define CAMERA_FREE_MOUSE_SENSITIVITY                   0.01f
#define UNDOBUFFER_SIZE 10000
#define ScreenWidth 800
#define ScreenHeight 450
#define TEXT_SIZE_MENU ScreenHeight/11
#define TEXT_SIZE ScreenWidth/40

int undo_buffer[UNDOBUFFER_SIZE];
int* p_undo = undo_buffer;
int* max_redo = undo_buffer;
int* min_redo = undo_buffer;
int rotating = -1;
int move_counter = 0;
int rot_multiplier = 1;
int scrambling = 0;
unsigned int CoolDown = 0;
Vector4 null = { 0.0f, 0.0f, 0.0f, 0.0f };
Vector4 rotation = { 0.0f, 0.0f, 0.0f, 0.0f };
double start_time;
double ongoing_time = 0.0;
double end_time = 0.0;
Camera3D camera;
Ray raycast;

struct Flags {
    unsigned int setup;
    unsigned int undoing;
    unsigned int redoing;
    unsigned int textured;
    unsigned int solved;
    unsigned int was_solved;
    unsigned int started;
    unsigned int successful_click;
} Flags;

typedef struct { // A cube element only needs 3 faces textured
    Texture2D* top;
    Texture2D* left;
    Texture2D* right;
    Vector3 position;
    Matrix matrix;
    Vector3 check; // used to detect solved state of a textured cube
} Cube;

typedef struct Face Face;

struct Face{ 
    Vector3 a; // Corners of a face to detect which one you click on. 
    Vector3 b;
    Vector3 c;
    Vector3 d;
    Vector3 center;
    Vector3 check; // used to detect solved state for untextured cube
    Face* neighbour1; // used to detect which rotation based on mouse movement
    Face* neighbour2;
    int rotation1;
    int rotation2;
};

typedef struct { // List the cubes in a side and the ones not in it. Helps drawing rotating cubes 
    Cube* c1;
    Cube* c2;
    Cube* c3;
    Cube* c4;
    Cube* c5;
    Cube* c6;
    Cube* c7;
    Cube* c8;
    Cube* c9;
    Cube* others[18];
    Face* face_faces; // 9 faces make a side
    Face* ring_faces[12]; // 12 faces surrounding the 9 main faces
} Side;

typedef struct { // Which rotation and side
    Side* side;
    Vector4 rot;
    Matrix* rot_matrix;
} lookup_table;

typedef struct {
    float targetDistance;           // Camera distance from position to target
    Vector2 angle;
    Vector2 previousMousePosition;  // Previous mouse position
    int rotationControl;            // Rotate view control key
} CustomCameraData;

static CustomCameraData C_CAMERA = {
    .targetDistance = 0,
    .previousMousePosition = { 0 },
    .rotationControl = 1            // raylib: MOUSE_BUTTON_RIGHT
};

Texture2D Textures[17];

// Cube indices from top left to down right, looking to the origin from the end of x/y/z
// The front face of the cube is made of Cubes 1 to 9 and rotates around the Z axis. 
// The back face ............................ 21 to 29 ............................
// Cube 15's center is the origin.
// z axis is positive going from cube 15 to 5
// y axis is positive going from cube 15 to 12
// x axis is positive going from cube 15 to 16

Cube Cubes[30];
Face* Faces[54];
Face Up_Faces[9];       // Cubes {21 22 23 11 12 13 1 2 3}
Face Down_Faces[9];     // Cubes {27 28 29 17 18 19 7 8 9}
Face Left_Faces[9];     // Cubes {1 11 21 4 14 24 7 17 27}
Face Right_Faces[9];    // Cubes {3 13 23 6 16 26 9 19 29}
Face Front_Faces[9];    // Cubes {1 2 3 4 5 6 7 8 9}
Face Back_Faces[9];     // Cubes {21 22 23 24 25 26 27 28 29}
Face* rotating_face;
Face** p_faces;

Image red_i;
Image green_i;
Image white_i;
Image orange_i;
Image blue_i;
Image yellow_i;

Matrix identity;
Matrix x_axis_CW;
Matrix x_axis_CCW;
Matrix y_axis_CW;
Matrix y_axis_CCW;
Matrix z_axis_CW;
Matrix z_axis_CCW;

Side front; 
Side mid_front; 
Side back; 
Side left;  
Side mid_side;     
Side right;    
Side top;    
Side mid_h;   
Side bottom;
Side* rotating_side;

lookup_table rotation_table[18];

void InitRotationMatrices()
{ 
    float theta = M_PI_2;
    x_axis_CW.m0 = 1.0f;        x_axis_CW.m4 = 0.0f;        x_axis_CW.m8 = 0.0f;        x_axis_CW.m12 = 0.0f;
    x_axis_CW.m1 = 0.0f;        x_axis_CW.m5 = cos(theta);  x_axis_CW.m9 = -sin(theta); x_axis_CW.m13 = 0.0f;
    x_axis_CW.m2 = 0.0f;        x_axis_CW.m6 = sin(theta);  x_axis_CW.m10 = cos(theta); x_axis_CW.m14 = 0.0f;
    x_axis_CW.m3 = 0.0f;        x_axis_CW.m7 = 0.0f;        x_axis_CW.m11 = 0.0f;       x_axis_CW.m15 = 1.0f;

    y_axis_CW.m0 = cos(theta);  y_axis_CW.m4 = 0.0f;        y_axis_CW.m8 = sin(theta);  y_axis_CW.m12 = 0.0f;
    y_axis_CW.m1 = 0.0f;        y_axis_CW.m5 = 1.0f;        y_axis_CW.m9 = 0.0f;        y_axis_CW.m13 = 0.0f;
    y_axis_CW.m2 = -sin(theta); y_axis_CW.m6 = 0.0f;        y_axis_CW.m10 = cos(theta); y_axis_CW.m14 = 0.0f;
    y_axis_CW.m3 = 0.0f;        y_axis_CW.m7 = 0.0f;        y_axis_CW.m11 = 0.0f;       y_axis_CW.m15 = 1.0f;

    z_axis_CW.m0 = cos(theta);  z_axis_CW.m4 = -sin(theta); z_axis_CW.m8 = 0.0f;        z_axis_CW.m12 = 0.0f;
    z_axis_CW.m1 = sin(theta);  z_axis_CW.m5 = cos(theta);  z_axis_CW.m9 = 0.0f;        z_axis_CW.m13 = 0.0f;
    z_axis_CW.m2 = 0.0f;        z_axis_CW.m6 = 0.0f;        z_axis_CW.m10 = 1.0f;       z_axis_CW.m14 = 0.0f;
    z_axis_CW.m3 = 0.0f;        z_axis_CW.m7 = 0.0f;        z_axis_CW.m11 = 0.0f;       z_axis_CW.m15 = 1.0f;

    theta = 3 * M_PI_2;
    x_axis_CCW.m0 = 1.0f;        x_axis_CCW.m4 = 0.0f;        x_axis_CCW.m8 = 0.0f;        x_axis_CCW.m12 = 0.0f;
    x_axis_CCW.m1 = 0.0f;        x_axis_CCW.m5 = cos(theta);  x_axis_CCW.m9 = -sin(theta); x_axis_CCW.m13 = 0.0f;
    x_axis_CCW.m2 = 0.0f;        x_axis_CCW.m6 = sin(theta);  x_axis_CCW.m10 = cos(theta); x_axis_CCW.m14 = 0.0f;
    x_axis_CCW.m3 = 0.0f;        x_axis_CCW.m7 = 0.0f;        x_axis_CCW.m11 = 0.0f;       x_axis_CCW.m15 = 1.0f;

    y_axis_CCW.m0 = cos(theta);  y_axis_CCW.m4 = 0.0f;        y_axis_CCW.m8 = sin(theta);  y_axis_CCW.m12 = 0.0f;
    y_axis_CCW.m1 = 0.0f;        y_axis_CCW.m5 = 1.0f;        y_axis_CCW.m9 = 0.0f;        y_axis_CCW.m13 = 0.0f;
    y_axis_CCW.m2 = -sin(theta); y_axis_CCW.m6 = 0.0f;        y_axis_CCW.m10 = cos(theta); y_axis_CCW.m14 = 0.0f;
    y_axis_CCW.m3 = 0.0f;        y_axis_CCW.m7 = 0.0f;        y_axis_CCW.m11 = 0.0f;       y_axis_CCW.m15 = 1.0f;

    z_axis_CCW.m0 = cos(theta);  z_axis_CCW.m4 = -sin(theta); z_axis_CCW.m8 = 0.0f;        z_axis_CCW.m12 = 0.0f;
    z_axis_CCW.m1 = sin(theta);  z_axis_CCW.m5 = cos(theta);  z_axis_CCW.m9 = 0.0f;        z_axis_CCW.m13 = 0.0f;
    z_axis_CCW.m2 = 0.0f;        z_axis_CCW.m6 = 0.0f;        z_axis_CCW.m10 = 1.0f;       z_axis_CCW.m14 = 0.0f;
    z_axis_CCW.m3 = 0.0f;        z_axis_CCW.m7 = 0.0f;        z_axis_CCW.m11 = 0.0f;       z_axis_CCW.m15 = 1.0f;

    identity.m0 = 1.0f;          identity.m4 = 0.0f;          identity.m8 = 0.0f;          identity.m12 = 0.0f;
    identity.m1 = 0.0f;          identity.m5 = 1.0f;          identity.m9 = 0.0f;          identity.m13 = 0.0f;
    identity.m2 = 0.0f;          identity.m6 = 0.0f;          identity.m10 = 1.0f;         identity.m14 = 0.0f;
    identity.m3 = 0.0f;          identity.m7 = 0.0f;          identity.m11 = 0.0f;         identity.m15 = 1.0f;
}

void InitTextures()
{ // Load and rotate textures
    Textures[0] = LoadTexture("resources/black.png");

    if (Flags.textured) {
        red_i = LoadImage("resources/z1.png");
        blue_i = LoadImage("resources/z2.png");
        green_i = LoadImage("resources/z3.png");
        yellow_i = LoadImage("resources/z4.png");
        orange_i = LoadImage("resources/z5.png");
        white_i = LoadImage("resources/z6.png");
    }

    else {
        red_i = LoadImage("resources/red.png");
        blue_i = LoadImage("resources/blue.png");
        green_i = LoadImage("resources/green.png");
        yellow_i = LoadImage("resources/yellow.png");
        orange_i = LoadImage("resources/orange.png");
        white_i = LoadImage("resources/white.png");
    }

    Textures[1] = LoadTextureFromImage(red_i);
    ImageRotateCCW(&red_i);
    ImageRotateCCW(&red_i);
    Textures[2] = LoadTextureFromImage(red_i);

    Textures[3] = LoadTextureFromImage(blue_i);
    ImageRotateCCW(&blue_i);
    ImageRotateCCW(&blue_i);
    Textures[4] = LoadTextureFromImage(blue_i);

    Textures[5] = LoadTextureFromImage(green_i);
    ImageRotateCCW(&green_i);
    ImageRotateCCW(&green_i);
    Textures[6] = LoadTextureFromImage(green_i);

    Textures[7] = LoadTextureFromImage(yellow_i);
    ImageRotateCCW(&yellow_i);
    Textures[8] = LoadTextureFromImage(yellow_i);
    ImageRotateCCW(&yellow_i);
    Textures[9] = LoadTextureFromImage(yellow_i);
    ImageRotateCCW(&yellow_i);
    Textures[10] = LoadTextureFromImage(yellow_i);

    Textures[11] = LoadTextureFromImage(orange_i);
    ImageRotateCCW(&orange_i);
    ImageRotateCCW(&orange_i);
    Textures[12] = LoadTextureFromImage(orange_i);

    Textures[13] = LoadTextureFromImage(white_i);
    ImageRotateCCW(&white_i);
    Textures[14] = LoadTextureFromImage(white_i);
    ImageRotateCCW(&white_i);
    Textures[15] = LoadTextureFromImage(white_i);
    ImageRotateCCW(&white_i);
    Textures[16] = LoadTextureFromImage(white_i);
}

void InitCubes()
{ // Set up all cubes with proper texture, position and orientation
    Cubes[1].top = &Textures[13];
    Cubes[1].left = &Textures[6];
    Cubes[1].right = &Textures[2];
    Cubes[1].position = (Vector3){ -2.0f, 2.0f, 2.0f };
    Cubes[1].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[1].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[2].top = &Textures[13];
    Cubes[2].left = &Textures[0];
    Cubes[2].right = &Textures[2];
    Cubes[2].position = (Vector3){ 0.0f, 2.0f, 2.0f };
    Cubes[2].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[2].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[3].top = &Textures[14];
    Cubes[3].left = &Textures[2];
    Cubes[3].right = &Textures[4];
    Cubes[3].position = (Vector3){ 2.0f, 2.0f, 2.0f };
    Cubes[3].matrix = identity;
    Cubes[3].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[4].top = &Textures[0];
    Cubes[4].left = &Textures[6];
    Cubes[4].right = &Textures[2];
    Cubes[4].position = (Vector3){ -2.0f, 0.0f, 2.0f };
    Cubes[4].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[4].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[5].top = &Textures[0];
    Cubes[5].left = &Textures[0];
    Cubes[5].right = &Textures[2];
    Cubes[5].position = (Vector3){ 0.0f, 0.0f, 2.0f };
    Cubes[5].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[5].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[6].top = &Textures[0];
    Cubes[6].left = &Textures[2];
    Cubes[6].right = &Textures[4];
    Cubes[6].position = (Vector3){ 2.0f, 0.0f, 2.0f };
    Cubes[6].matrix = identity;
    Cubes[6].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[7].top = &Textures[7];
    Cubes[7].left = &Textures[1];
    Cubes[7].right = &Textures[5];
    Cubes[7].position = (Vector3){ -2.0f, -2.0f, 2.0f };
    Cubes[7].matrix = MatrixMultiply(z_axis_CW, identity);
    Cubes[7].matrix = MatrixMultiply(z_axis_CW, Cubes[7].matrix);
    Cubes[7].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[8].top = &Textures[7];
    Cubes[8].left = &Textures[1];
    Cubes[8].right = &Textures[0];
    Cubes[8].position = (Vector3){ 0.0f, -2.0f, 2.0f };
    Cubes[8].matrix = MatrixMultiply(z_axis_CW, identity);
    Cubes[8].matrix = MatrixMultiply(z_axis_CW, Cubes[8].matrix);
    Cubes[8].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[9].top = &Textures[10];
    Cubes[9].left = &Textures[3];
    Cubes[9].right = &Textures[1];
    Cubes[9].position = (Vector3){ 2.0f, -2.0f, 2.0f };
    Cubes[9].matrix = MatrixMultiply(z_axis_CW, identity);
    Cubes[9].matrix = MatrixMultiply(z_axis_CW, Cubes[9].matrix);
    Cubes[9].matrix = MatrixMultiply(y_axis_CCW, Cubes[9].matrix);
    Cubes[9].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[11].top = &Textures[13];
    Cubes[11].left = &Textures[6];
    Cubes[11].right = &Textures[0];
    Cubes[11].position = (Vector3){ -2.0f, 2.0f, 0.0f };
    Cubes[11].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[11].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[12].top = &Textures[14];
    Cubes[12].left = &Textures[0];
    Cubes[12].right = &Textures[0];
    Cubes[12].position = (Vector3){ 0.0f, 2.0f, 0.0f };
    Cubes[12].matrix = identity;
    Cubes[12].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[13].top = &Textures[14];
    Cubes[13].left = &Textures[0];
    Cubes[13].right = &Textures[4];
    Cubes[13].position = (Vector3){ 2.0f, 2.0f, 0.0f };
    Cubes[13].matrix = identity;
    Cubes[13].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[14].top = &Textures[0];
    Cubes[14].left = &Textures[6];
    Cubes[14].right = &Textures[0];
    Cubes[14].position = (Vector3){ -2.0f, 0.0f, 0.0f };
    Cubes[14].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[14].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[15].top = &Textures[0];
    Cubes[15].left = &Textures[0];
    Cubes[15].right = &Textures[0];
    Cubes[15].position = (Vector3){ 0.0f, 0.0f, 0.0f };
    Cubes[15].matrix = identity;
    Cubes[15].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[16].top = &Textures[0];
    Cubes[16].left = &Textures[0];
    Cubes[16].right = &Textures[4];
    Cubes[16].position = (Vector3){ 2.0f, 0.0f, 0.0f };
    Cubes[16].matrix = identity;
    Cubes[16].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[17].top = &Textures[7];
    Cubes[17].left = &Textures[0];
    Cubes[17].right = &Textures[5];
    Cubes[17].position = (Vector3){ -2.0f, -2.0f, 0.0f };
    Cubes[17].matrix = MatrixMultiply(z_axis_CW, identity);
    Cubes[17].matrix = MatrixMultiply(z_axis_CW, Cubes[17].matrix);
    Cubes[17].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[18].top = &Textures[7];
    Cubes[18].left = &Textures[0];
    Cubes[18].right = &Textures[0];
    Cubes[18].position = (Vector3){ 0.0f, -2.0f, 0.0f };
    Cubes[18].matrix = MatrixMultiply(z_axis_CW, identity);
    Cubes[18].matrix = MatrixMultiply(z_axis_CW, Cubes[18].matrix);
    Cubes[18].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[19].top = &Textures[10];
    Cubes[19].left = &Textures[3];
    Cubes[19].right = &Textures[0];
    Cubes[19].position = (Vector3){ 2.0f, -2.0f, 0.0f };
    Cubes[19].matrix = MatrixMultiply(z_axis_CW, identity);
    Cubes[19].matrix = MatrixMultiply(z_axis_CW, Cubes[19].matrix);
    Cubes[19].matrix = MatrixMultiply(y_axis_CCW, Cubes[19].matrix);
    Cubes[19].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[21].top = &Textures[16];
    Cubes[21].left = &Textures[12];
    Cubes[21].right = &Textures[6];
    Cubes[21].position = (Vector3){ -2.0f, 2.0f, -2.0f };
    Cubes[21].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[21].matrix = MatrixMultiply(y_axis_CW, Cubes[21].matrix);
    Cubes[21].check = (Vector3){ 10.0f, 10.0f, 10.0f };
    
    Cubes[22].top = &Textures[16];
    Cubes[22].left = &Textures[12];
    Cubes[22].right = &Textures[0];
    Cubes[22].position = (Vector3){ 0.0f, 2.0f, -2.0f };
    Cubes[22].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[22].matrix = MatrixMultiply(y_axis_CW, Cubes[22].matrix);
    Cubes[22].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[23].top = &Textures[15];
    Cubes[23].left = &Textures[4];
    Cubes[23].right = &Textures[12];
    Cubes[23].position = (Vector3){ 2.0f, 2.0f, -2.0f };
    Cubes[23].matrix = MatrixMultiply(y_axis_CCW, identity);
    Cubes[23].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[24].top = &Textures[0];
    Cubes[24].left = &Textures[12];
    Cubes[24].right = &Textures[6];
    Cubes[24].position = (Vector3){ -2.0f, 0.0f, -2.0f };
    Cubes[24].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[24].matrix = MatrixMultiply(y_axis_CW, Cubes[24].matrix);
    Cubes[24].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[25].top = &Textures[0];
    Cubes[25].left = &Textures[12];
    Cubes[25].right = &Textures[0];
    Cubes[25].position = (Vector3){ 0.0f, 0.0f, -2.0f };
    Cubes[25].matrix = MatrixMultiply(y_axis_CW, identity);
    Cubes[25].matrix = MatrixMultiply(y_axis_CW, Cubes[25].matrix);
    Cubes[25].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[26].top = &Textures[0];
    Cubes[26].left = &Textures[4];
    Cubes[26].right = &Textures[12];
    Cubes[26].position = (Vector3){ 2.0f, 0.0f, -2.0f };
    Cubes[26].matrix = MatrixMultiply(y_axis_CCW, identity);
    Cubes[26].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[27].top = &Textures[8];
    Cubes[27].left = &Textures[5];
    Cubes[27].right = &Textures[11];
    Cubes[27].position = (Vector3){ -2.0f, -2.0f, -2.0f };
    Cubes[27].matrix = MatrixMultiply(x_axis_CW, identity);
    Cubes[27].matrix = MatrixMultiply(x_axis_CW, Cubes[27].matrix);
    Cubes[27].matrix = MatrixMultiply(y_axis_CCW, Cubes[27].matrix);
    Cubes[27].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[28].top = &Textures[8];
    Cubes[28].left = &Textures[0];
    Cubes[28].right = &Textures[11];
    Cubes[28].position = (Vector3){ 0.0f, -2.0f, -2.0f };
    Cubes[28].matrix = MatrixMultiply(x_axis_CW, identity);
    Cubes[28].matrix = MatrixMultiply(x_axis_CW, Cubes[28].matrix);
    Cubes[28].matrix = MatrixMultiply(y_axis_CCW, Cubes[28].matrix);
    Cubes[28].check = (Vector3){ 10.0f, 10.0f, 10.0f };

    Cubes[29].top = &Textures[9];
    Cubes[29].left = &Textures[11];
    Cubes[29].right = &Textures[3];
    Cubes[29].position = (Vector3){ 2.0f, -2.0f, -2.0f };
    Cubes[29].matrix = MatrixMultiply(x_axis_CW, identity);
    Cubes[29].matrix = MatrixMultiply(x_axis_CW, Cubes[29].matrix);
    Cubes[29].check = (Vector3){ 10.0f, 10.0f, 10.0f };
}

void InitFaces()   
{   // Cube indices from top left to down right, looking to the origin from the end of x/y/z 
    // Up faces
    p_faces = &Faces;
    int t_indices[9] = { 21, 22, 23, 11, 12, 13, 1, 2, 3 };
    for (int i = 0; i < 9; i++) {
        *p_faces++ = &Up_Faces[i];
        int j = t_indices[i];
        float x = Cubes[j].position.x;
        float y = Cubes[j].position.y;
        float z = Cubes[j].position.z;
        Up_Faces[i].a = (Vector3){x - 1, y + 1, z + 1};
        Up_Faces[i].b = (Vector3){ x - 1, y + 1, z - 1};
        Up_Faces[i].c = (Vector3){ x + 1, y + 1, z - 1 };
        Up_Faces[i].d = (Vector3){ x + 1, y + 1, z + 1 };
        Up_Faces[i].center = (Vector3){ x, y + 1, z };
        Up_Faces[i].check = (Vector3){ 0,1,0 };
        if (j == 21) {
        Up_Faces[i].rotation1 = 15;                  Up_Faces[i].rotation2 = 2;
        Up_Faces[i].neighbour1 = &Up_Faces[3]; //11 up     
        Up_Faces[i].neighbour2 = &Up_Faces[1]; //22 up
        }
        else if (j == 22) {
        Up_Faces[i].rotation1 = 16;                  Up_Faces[i].rotation2 = 2;
        Up_Faces[i].neighbour1 = &Up_Faces[4]; //12 up     
        Up_Faces[i].neighbour2 = &Up_Faces[2]; //23 up
        }
        else if (j == 23) {
        Up_Faces[i].rotation1 = 17;                  Up_Faces[i].rotation2 = 11;
        Up_Faces[i].neighbour1 = &Up_Faces[5]; //13 up     
        Up_Faces[i].neighbour2 = &Up_Faces[1]; //22 up
        }
        else if (j == 11) {
            Up_Faces[i].rotation1 = 6;                  Up_Faces[i].rotation2 = 1;
            Up_Faces[i].neighbour1 = &Up_Faces[0]; //21 up     
            Up_Faces[i].neighbour2 = &Up_Faces[4]; //12 up
        }
        else if (j == 12) {
            Up_Faces[i].rotation1 = 7;                  Up_Faces[i].rotation2 = 1;
            Up_Faces[i].neighbour1 = &Up_Faces[1]; //22 up  
            Up_Faces[i].neighbour2 = &Up_Faces[5]; //13 up
        }
        else if (j == 13) {
            Up_Faces[i].rotation1 = 8;                  Up_Faces[i].rotation2 = 10;
            Up_Faces[i].neighbour1 = &Up_Faces[2]; //23 up     
            Up_Faces[i].neighbour2 = &Up_Faces[4]; //12 up
        }
        else if (j == 1) {
            Up_Faces[i].rotation1 = 6;                  Up_Faces[i].rotation2 = 0;
            Up_Faces[i].neighbour1 = &Up_Faces[3]; //11 up     
            Up_Faces[i].neighbour2 = &Up_Faces[7]; //2 up
        }
        else if (j == 2) {
            Up_Faces[i].rotation1 = 7;                  Up_Faces[i].rotation2 = 0;
            Up_Faces[i].neighbour1 = &Up_Faces[4]; //12 up  
            Up_Faces[i].neighbour2 = &Up_Faces[8]; //3 up
        }
        else {
            Up_Faces[i].rotation1 = 8;                  Up_Faces[i].rotation2 = 9;
            Up_Faces[i].neighbour1 = &Up_Faces[5]; //13 up     
            Up_Faces[i].neighbour2 = &Up_Faces[7]; //2 up
        }
    }

    // Down faces
    int d_indices[9] = { 27, 28, 29, 17, 18, 19, 7, 8, 9 };
    for (int i = 0; i < 9; i++) {
        *p_faces++ = &Down_Faces[i];
        int j = d_indices[i];
        float x = Cubes[j].position.x;
        float y = Cubes[j].position.y;
        float z = Cubes[j].position.z;
        Down_Faces[i].a = (Vector3){ x - 1, y - 1, z + 1 };
        Down_Faces[i].b = (Vector3){ x - 1, y - 1, z - 1 };
        Down_Faces[i].c = (Vector3){ x + 1, y - 1, z - 1 };
        Down_Faces[i].d = (Vector3){ x + 1, y - 1, z + 1 };
        Down_Faces[i].center = (Vector3){ x, y - 1, z };
        Down_Faces[i].check = (Vector3){ 0, -1, 0 };
        if (j == 27) {
            Down_Faces[i].rotation1 = 6;               Down_Faces[i].rotation2 = 11;
            Down_Faces[i].neighbour1 = &Down_Faces[3]; //17 down
            Down_Faces[i].neighbour2 = &Down_Faces[1]; //28 down
        }
        else if (j == 28) {
            Down_Faces[i].rotation1 = 7;               Down_Faces[i].rotation2 = 11;
            Down_Faces[i].neighbour1 = &Down_Faces[4]; //18 down
            Down_Faces[i].neighbour2 = &Down_Faces[2]; //29 down
        }
        else if (j == 29) {
            Down_Faces[i].rotation1 = 8;               Down_Faces[i].rotation2 = 2;
            Down_Faces[i].neighbour1 = &Down_Faces[5]; //19 down
            Down_Faces[i].neighbour2 = &Down_Faces[1]; //28 down
        }
        else if (j == 17) {
            Down_Faces[i].rotation1 = 15;               Down_Faces[i].rotation2 = 10;
            Down_Faces[i].neighbour1 = &Down_Faces[0]; //27 down
            Down_Faces[i].neighbour2 = &Down_Faces[4]; //18 down
        }
        else if (j == 18) {
            Down_Faces[i].rotation1 = 16;               Down_Faces[i].rotation2 = 10;
            Down_Faces[i].neighbour1 = &Down_Faces[1]; //28 down
            Down_Faces[i].neighbour2 = &Down_Faces[5]; //19 down
        }
        else if (j == 19) {
            Down_Faces[i].rotation1 = 17;               Down_Faces[i].rotation2 = 1;
            Down_Faces[i].neighbour1 = &Down_Faces[2]; //29 down
            Down_Faces[i].neighbour2 = &Down_Faces[4]; //18 down
        }
        else if (j == 7) {
            Down_Faces[i].rotation1 = 15;               Down_Faces[i].rotation2 = 9;
            Down_Faces[i].neighbour1 = &Down_Faces[3]; //17 down
            Down_Faces[i].neighbour2 = &Down_Faces[7]; //8 down
        }
        else if (j == 8) {
            Down_Faces[i].rotation1 = 16;               Down_Faces[i].rotation2 = 9;
            Down_Faces[i].neighbour1 = &Down_Faces[4]; //18 down
            Down_Faces[i].neighbour2 = &Down_Faces[8]; //9 down
        }
        else {
            Down_Faces[i].rotation1 = 17;               Down_Faces[i].rotation2 = 0;
            Down_Faces[i].neighbour1 = &Down_Faces[5]; //19 down
            Down_Faces[i].neighbour2 = &Down_Faces[7]; //8 down
        }
    }

    // Front faces
    int f_indices[9] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    for (int i = 0; i < 9; i++) {
        *p_faces++ = &Front_Faces[i];
        int j = f_indices[i];
        float x = Cubes[j].position.x;
        float y = Cubes[j].position.y;
        float z = Cubes[j].position.z;
        Front_Faces[i].a = (Vector3){x - 1, y + 1, z + 1};
        Front_Faces[i].b = (Vector3){ x - 1, y - 1, z + 1 };
        Front_Faces[i].c = (Vector3){ x + 1, y - 1, z + 1 };
        Front_Faces[i].d = (Vector3){ x + 1, y + 1, z + 1 };
        Front_Faces[i].center = (Vector3){ x, y, z + 1 };
        Front_Faces[i].check = (Vector3){ 0, 0, 1 };
        if (j == 1) {
            Front_Faces[i].rotation1 = 15;              Front_Faces[i].rotation2 = 12;
            Front_Faces[i].neighbour1 = &Front_Faces[3]; //4 front
            Front_Faces[i].neighbour2 = &Front_Faces[1]; //2 front
        }
        else if (j == 2) {
            Front_Faces[i].rotation1 = 16;              Front_Faces[i].rotation2 = 12;
            Front_Faces[i].neighbour1 = &Front_Faces[4]; //5 front
            Front_Faces[i].neighbour2 = &Front_Faces[2]; //3 front
        }
        else if (j == 3) {
            Front_Faces[i].rotation1 = 17;              Front_Faces[i].rotation2 = 3;
            Front_Faces[i].neighbour1 = &Front_Faces[5]; //6 front
            Front_Faces[i].neighbour2 = &Front_Faces[1]; //2 front
        }
        else if (j == 4) {
            Front_Faces[i].rotation1 = 15;              Front_Faces[i].rotation2 = 13;
            Front_Faces[i].neighbour1 = &Front_Faces[6]; //7 front
            Front_Faces[i].neighbour2 = &Front_Faces[4]; //5 front
        }
        else if (j == 5) {
            Front_Faces[i].rotation1 = 16;              Front_Faces[i].rotation2 = 13;
            Front_Faces[i].neighbour1 = &Front_Faces[7]; //8 front
            Front_Faces[i].neighbour2 = &Front_Faces[5]; //6 front
        }
        else if (j == 6) {
            Front_Faces[i].rotation1 = 17;              Front_Faces[i].rotation2 = 4;
            Front_Faces[i].neighbour1 = &Front_Faces[8]; //9 front
            Front_Faces[i].neighbour2 = &Front_Faces[4]; //5 front
        }
        else if (j == 7) {
            Front_Faces[i].rotation1 = 6;              Front_Faces[i].rotation2 = 14;
            Front_Faces[i].neighbour1 = &Front_Faces[3]; //4 front
            Front_Faces[i].neighbour2 = &Front_Faces[7]; //8 front
        }
        else if (j == 8) {
            Front_Faces[i].rotation1 = 7;              Front_Faces[i].rotation2 = 14;
            Front_Faces[i].neighbour1 = &Front_Faces[4]; //5 front
            Front_Faces[i].neighbour2 = &Front_Faces[8]; //9 front
        }
        else {
            Front_Faces[i].rotation1 = 8;              Front_Faces[i].rotation2 = 5;
            Front_Faces[i].neighbour1 = &Front_Faces[5]; //6 front
            Front_Faces[i].neighbour2 = &Front_Faces[6]; //8 front
        }
    }

    // Back faces
    int b_indices[9] = { 21, 22, 23, 24, 25, 26, 27, 28, 29 };
    for (int i = 0; i < 9; i++) {
        *p_faces++ = &Back_Faces[i];
        int j = b_indices[i];
        float x = Cubes[j].position.x;
        float y = Cubes[j].position.y;
        float z = Cubes[j].position.z;
        Back_Faces[i].a = (Vector3){ x - 1, y + 1, z - 1 };
        Back_Faces[i].b = (Vector3){ x - 1, y - 1, z - 1 };
        Back_Faces[i].c = (Vector3){ x + 1, y - 1, z - 1 };
        Back_Faces[i].d = (Vector3){ x + 1, y + 1, z - 1 };
        Back_Faces[i].center = (Vector3){ x, y, z - 1 };
        Back_Faces[i].check = (Vector3){ 0, 0, -1 };
        if (j == 21) {
            Back_Faces[i].rotation1 = 6;                Back_Faces[i].rotation2 = 3;
            Back_Faces[i].neighbour1 = &Back_Faces[3]; //24 back
            Back_Faces[i].neighbour2 = &Back_Faces[1]; //22 back
        }
        else if (j == 22) {
            Back_Faces[i].rotation1 = 7;                Back_Faces[i].rotation2 = 3;
            Back_Faces[i].neighbour1 = &Back_Faces[4]; //25 back
            Back_Faces[i].neighbour2 = &Back_Faces[2]; //23 back
        }
        else if (j == 23) {
            Back_Faces[i].rotation1 = 8;                Back_Faces[i].rotation2 = 12;
            Back_Faces[i].neighbour1 = &Back_Faces[5]; //26 back
            Back_Faces[i].neighbour2 = &Back_Faces[1]; //22 back
        }
        else if (j == 24) {
            Back_Faces[i].rotation1 = 6;                Back_Faces[i].rotation2 = 4;
            Back_Faces[i].neighbour1 = &Back_Faces[6]; //27 back
            Back_Faces[i].neighbour2 = &Back_Faces[4]; //25 back
        }
        else if (j == 25) {
            Back_Faces[i].rotation1 = 7;                Back_Faces[i].rotation2 = 4;
            Back_Faces[i].neighbour1 = &Back_Faces[7]; //28 back
            Back_Faces[i].neighbour2 = &Back_Faces[5]; //26 back
        }
        else if (j == 26) {
            Back_Faces[i].rotation1 = 8;                Back_Faces[i].rotation2 = 13;
            Back_Faces[i].neighbour1 = &Back_Faces[8]; //29 back
            Back_Faces[i].neighbour2 = &Back_Faces[4]; //25 back
        }
        else if (j == 27) {
            Back_Faces[i].rotation1 = 15;                Back_Faces[i].rotation2 = 5;
            Back_Faces[i].neighbour1 = &Back_Faces[3]; //24 back
            Back_Faces[i].neighbour2 = &Back_Faces[7]; //28 back
        }
        else if (j == 28) {
            Back_Faces[i].rotation1 = 16;                Back_Faces[i].rotation2 = 5;
            Back_Faces[i].neighbour1 = &Back_Faces[4]; //25 back
            Back_Faces[i].neighbour2 = &Back_Faces[8]; //29 back
        }
        else {
            Back_Faces[i].rotation1 = 17;                Back_Faces[i].rotation2 = 14;
            Back_Faces[i].neighbour1 = &Back_Faces[5]; //26 back
            Back_Faces[i].neighbour2 = &Back_Faces[7]; //28 back
        }
    }

    // Left faces 
    int l_indices[9] = { 1, 11, 21, 4, 14, 24, 7, 17, 27 };
    for (int i = 0; i < 9; i++) {
        *p_faces++ = &Left_Faces[i];
        int j = l_indices[i];
        float x = Cubes[j].position.x;
        float y = Cubes[j].position.y;
        float z = Cubes[j].position.z;
        Left_Faces[i].a = (Vector3){ x - 1, y + 1, z - 1 };
        Left_Faces[i].b = (Vector3){ x - 1, y - 1, z - 1 };
        Left_Faces[i].c = (Vector3){ x - 1, y - 1, z + 1 };
        Left_Faces[i].d = (Vector3){ x - 1, y + 1, z + 1 };
        Left_Faces[i].center = (Vector3){ x - 1, y, z };
        Left_Faces[i].check = (Vector3){ -1, 0, 0 };
        if (j == 1) {
            Left_Faces[i].rotation1 = 3;                 Left_Faces[i].rotation2 = 9;
            Left_Faces[i].neighbour1 = &Left_Faces[1]; // 11 left
            Left_Faces[i].neighbour2 = &Left_Faces[3]; // 4 left
        }
        else if (j == 11) {
            Left_Faces[i].rotation1 = 3;                 Left_Faces[i].rotation2 = 10;
            Left_Faces[i].neighbour1 = &Left_Faces[2]; // 21 left
            Left_Faces[i].neighbour2 = &Left_Faces[4]; // 14 left
        }
        else if (j == 21) {
            Left_Faces[i].rotation1 = 12;                 Left_Faces[i].rotation2 = 11;
            Left_Faces[i].neighbour1 = &Left_Faces[1]; // 11 left
            Left_Faces[i].neighbour2 = &Left_Faces[5]; // 24 left
        }
        else if (j == 4) {
            Left_Faces[i].rotation1 = 4;                 Left_Faces[i].rotation2 = 9;
            Left_Faces[i].neighbour1 = &Left_Faces[4]; // 14 left
            Left_Faces[i].neighbour2 = &Left_Faces[6]; // 7 left
        }
        else if (j == 14) {
            Left_Faces[i].rotation1 = 4;                 Left_Faces[i].rotation2 = 10;
            Left_Faces[i].neighbour1 = &Left_Faces[5]; // 24 left
            Left_Faces[i].neighbour2 = &Left_Faces[7]; // 17 left
        }
        else if (j == 24) {
            Left_Faces[i].rotation1 = 13;                 Left_Faces[i].rotation2 = 11;
            Left_Faces[i].neighbour1 = &Left_Faces[4]; // 14 left
            Left_Faces[i].neighbour2 = &Left_Faces[8]; // 27 left
        }
        else if (j == 7) {
            Left_Faces[i].rotation1 = 5;                 Left_Faces[i].rotation2 = 0;
            Left_Faces[i].neighbour1 = &Left_Faces[7]; // 17 left
            Left_Faces[i].neighbour2 = &Left_Faces[3]; // 4 left
        }
        else if (j == 17) {
            Left_Faces[i].rotation1 = 5;                 Left_Faces[i].rotation2 = 1;
            Left_Faces[i].neighbour1 = &Left_Faces[8]; // 27 left
            Left_Faces[i].neighbour2 = &Left_Faces[4]; // 14 left
        }
        else {
            Left_Faces[i].rotation1 = 14;                 Left_Faces[i].rotation2 = 2;
            Left_Faces[i].neighbour1 = &Left_Faces[7]; // 17 left
            Left_Faces[i].neighbour2 = &Left_Faces[5]; // 24 left
        }
    }

    // Right faces
    int r_indices[9] = { 3, 13, 23, 6, 16, 26, 9, 19, 29 };
    for (int i = 0; i < 9; i++) {
        *p_faces++ = &Right_Faces[i];
        int j = r_indices[i];
        float x = Cubes[j].position.x;
        float y = Cubes[j].position.y;
        float z = Cubes[j].position.z;
        Right_Faces[i].a = (Vector3){ x + 1, y + 1, z - 1 };
        Right_Faces[i].b = (Vector3){ x + 1, y - 1, z - 1 };
        Right_Faces[i].c = (Vector3){ x + 1, y - 1, z + 1 };
        Right_Faces[i].d = (Vector3){ x + 1, y + 1, z + 1 };
        Right_Faces[i].center = (Vector3){ x + 1, y, z };
        Right_Faces[i].check = (Vector3){ 1, 0, 0 };
        if (j == 3) {
            Right_Faces[i].rotation1 = 12;               Right_Faces[i].rotation2 = 0;
            Right_Faces[i].neighbour1 = &Right_Faces[1]; // 13 right
            Right_Faces[i].neighbour2 = &Right_Faces[3]; // 6 right
        }
        else if (j == 13) {
            Right_Faces[i].rotation1 = 12;               Right_Faces[i].rotation2 = 1;
            Right_Faces[i].neighbour1 = &Right_Faces[2]; // 23 right
            Right_Faces[i].neighbour2 = &Right_Faces[4]; // 16 right
        }
        else if (j == 23) {
            Right_Faces[i].rotation1 = 3;               Right_Faces[i].rotation2 = 2;
            Right_Faces[i].neighbour1 = &Right_Faces[1]; // 13 right
            Right_Faces[i].neighbour2 = &Right_Faces[5]; // 26 right
        }
        else if (j == 6) {
            Right_Faces[i].rotation1 = 13;               Right_Faces[i].rotation2 = 0;
            Right_Faces[i].neighbour1 = &Right_Faces[4]; // 16 right
            Right_Faces[i].neighbour2 = &Right_Faces[6]; // 9 right
        }
        else if (j == 16) {
            Right_Faces[i].rotation1 = 13;               Right_Faces[i].rotation2 = 1;
            Right_Faces[i].neighbour1 = &Right_Faces[5]; // 26 right
            Right_Faces[i].neighbour2 = &Right_Faces[7]; // 19 right
        }
        else if (j == 26) {
            Right_Faces[i].rotation1 = 4;               Right_Faces[i].rotation2 = 2;
            Right_Faces[i].neighbour1 = &Right_Faces[4]; // 16 right
            Right_Faces[i].neighbour2 = &Right_Faces[8]; // 29 right
        }
        else if (j == 9) {
            Right_Faces[i].rotation1 = 14;               Right_Faces[i].rotation2 = 9;
            Right_Faces[i].neighbour1 = &Right_Faces[7]; // 19 right
            Right_Faces[i].neighbour2 = &Right_Faces[3]; // 6 right
        }
        else if (j == 19) {
            Right_Faces[i].rotation1 = 14;               Right_Faces[i].rotation2 = 10;
            Right_Faces[i].neighbour1 = &Right_Faces[8]; // 29 right
            Right_Faces[i].neighbour2 = &Right_Faces[4]; // 16 right
        }
        else {
            Right_Faces[i].rotation1 = 5;               Right_Faces[i].rotation2 = 11;
            Right_Faces[i].neighbour1 = &Right_Faces[7]; // 19 right
            Right_Faces[i].neighbour2 = &Right_Faces[5]; // 26 right
        }
    }
}

void InitSides()
{ // Set up all side related data
    Face** ring_p;
    front.c1 = &Cubes[1];
    front.c2 = &Cubes[2];
    front.c3 = &Cubes[3];
    front.c4 = &Cubes[4];
    front.c5 = &Cubes[5];
    front.c6 = &Cubes[6];
    front.c7 = &Cubes[7];
    front.c8 = &Cubes[8];
    front.c9 = &Cubes[9];
    front.others[0] = &Cubes[11]; front.others[1] = &Cubes[12]; front.others[2] = &Cubes[13];
    front.others[3] = &Cubes[14]; front.others[4] = &Cubes[15]; front.others[5] = &Cubes[16];
    front.others[6] = &Cubes[17]; front.others[7] = &Cubes[18]; front.others[8] = &Cubes[19];
    front.others[9] = &Cubes[21]; front.others[10] = &Cubes[22]; front.others[11] = &Cubes[23];
    front.others[12] = &Cubes[24]; front.others[13] = &Cubes[25]; front.others[14] = &Cubes[26];
    front.others[15] = &Cubes[27]; front.others[16] = &Cubes[28]; front.others[17] = &Cubes[29];
    front.face_faces = &Front_Faces;
    ring_p = &front.ring_faces;
    *ring_p++ = &Up_Faces[6];     *ring_p++ = &Up_Faces[7]; *ring_p++ = &Up_Faces[8]; // up 1 2 3
    *ring_p++ = &Right_Faces[0];  *ring_p++ = &Right_Faces[3]; *ring_p++ = &Right_Faces[6]; // right 3 6 9
    *ring_p++ = &Down_Faces[8];   *ring_p++ = &Down_Faces[7]; *ring_p++ = &Down_Faces[6]; // down 9 8 7
    *ring_p++ = &Left_Faces[6];   *ring_p++ = &Left_Faces[3]; *ring_p++ = &Left_Faces[0]; // left 7 4 1

    mid_front.c1 = &Cubes[11];
    mid_front.c2 = &Cubes[12];
    mid_front.c3 = &Cubes[13];
    mid_front.c4 = &Cubes[14];
    mid_front.c5 = &Cubes[15];
    mid_front.c6 = &Cubes[16];
    mid_front.c7 = &Cubes[17];
    mid_front.c8 = &Cubes[18];
    mid_front.c9 = &Cubes[19];
    mid_front.others[0] = &Cubes[1]; mid_front.others[1] = &Cubes[2]; mid_front.others[2] = &Cubes[3];
    mid_front.others[3] = &Cubes[4]; mid_front.others[4] = &Cubes[5]; mid_front.others[5] = &Cubes[6];
    mid_front.others[6] = &Cubes[7]; mid_front.others[7] = &Cubes[8]; mid_front.others[8] = &Cubes[9];
    mid_front.others[9] = &Cubes[21]; mid_front.others[10] = &Cubes[22]; mid_front.others[11] = &Cubes[23];
    mid_front.others[12] = &Cubes[24]; mid_front.others[13] = &Cubes[25]; mid_front.others[14] = &Cubes[26];
    mid_front.others[15] = &Cubes[27]; mid_front.others[16] = &Cubes[28]; mid_front.others[17] = &Cubes[29];
    mid_front.face_faces = NULL;
    ring_p = &mid_front.ring_faces;
    *ring_p++ = &Up_Faces[3];    *ring_p++ = &Up_Faces[4]; *ring_p++ = &Up_Faces[5]; // up 11 12 13
    *ring_p++ = &Right_Faces[1]; *ring_p++ = &Right_Faces[4]; *ring_p++ = &Right_Faces[7]; // right 13 16 19
    *ring_p++ = &Down_Faces[5];  *ring_p++ = &Down_Faces[4]; *ring_p++ = &Down_Faces[3]; // down 19 18 17
    *ring_p++ = &Left_Faces[7];  *ring_p++ = &Left_Faces[4]; *ring_p++ = &Left_Faces[1]; // left 17 14 11

    back.c1 = &Cubes[21];
    back.c2 = &Cubes[22];
    back.c3 = &Cubes[23];
    back.c4 = &Cubes[24];
    back.c5 = &Cubes[25];
    back.c6 = &Cubes[26];
    back.c7 = &Cubes[27];
    back.c8 = &Cubes[28];
    back.c9 = &Cubes[29];
    back.others[0] = &Cubes[11]; back.others[1] = &Cubes[12]; back.others[2] = &Cubes[13];
    back.others[3] = &Cubes[14]; back.others[4] = &Cubes[15]; back.others[5] = &Cubes[16];
    back.others[6] = &Cubes[17]; back.others[7] = &Cubes[18]; back.others[8] = &Cubes[19];
    back.others[9] = &Cubes[1]; back.others[10] = &Cubes[2]; back.others[11] = &Cubes[3];
    back.others[12] = &Cubes[4]; back.others[13] = &Cubes[5]; back.others[14] = &Cubes[6];
    back.others[15] = &Cubes[7]; back.others[16] = &Cubes[8]; back.others[17] = &Cubes[9];
    back.face_faces = &Back_Faces;
    ring_p = &back.ring_faces;
    *ring_p++ = &Up_Faces[0];    *ring_p++ = &Up_Faces[1]; *ring_p++ = &Up_Faces[2]; // up 21 22 23
    *ring_p++ = &Right_Faces[2]; *ring_p++ = &Right_Faces[5]; *ring_p++ = &Right_Faces[8]; // right 23 26 29
    *ring_p++ = &Down_Faces[2]; *ring_p++ = &Down_Faces[1];  *ring_p++ = &Down_Faces[0]; // down 29 28 27
    *ring_p++ = &Left_Faces[8]; *ring_p++ = &Left_Faces[5];  *ring_p++ = &Left_Faces[2]; // left 27 24 21

    left.c1 = &Cubes[1];
    left.c2 = &Cubes[11];
    left.c3 = &Cubes[21];
    left.c4 = &Cubes[4];
    left.c5 = &Cubes[14];
    left.c6 = &Cubes[24];
    left.c7 = &Cubes[7];
    left.c8 = &Cubes[17];
    left.c9 = &Cubes[27];
    left.others[0] = &Cubes[22]; left.others[1] = &Cubes[12]; left.others[2] = &Cubes[2];
    left.others[3] = &Cubes[23]; left.others[4] = &Cubes[13]; left.others[5] = &Cubes[3];
    left.others[6] = &Cubes[25]; left.others[7] = &Cubes[15]; left.others[8] = &Cubes[5];
    left.others[9] = &Cubes[26]; left.others[10] = &Cubes[16]; left.others[11] = &Cubes[6];
    left.others[12] = &Cubes[28]; left.others[13] = &Cubes[18]; left.others[14] = &Cubes[8];
    left.others[15] = &Cubes[29]; left.others[16] = &Cubes[19]; left.others[17] = &Cubes[9];
    left.face_faces = &Left_Faces;
    ring_p = &left.ring_faces;
    *ring_p++ = &Up_Faces[6]; *ring_p++ = &Up_Faces[3]; *ring_p++ = &Up_Faces[0]; // up 1 11 21
    *ring_p++ = &Back_Faces[0]; *ring_p++ = &Back_Faces[3]; *ring_p++ = &Back_Faces[6]; // back 21 24 27
    *ring_p++ = &Down_Faces[0]; *ring_p++ = &Down_Faces[3]; *ring_p++ = &Down_Faces[6]; // down 27 17 7
    *ring_p++ = &Front_Faces[6]; *ring_p++ = &Front_Faces[3]; *ring_p++ = &Front_Faces[0]; // front 7 4 1
    
    mid_side.c1 = &Cubes[2];
    mid_side.c2 = &Cubes[12];
    mid_side.c3 = &Cubes[22];
    mid_side.c4 = &Cubes[5];
    mid_side.c5 = &Cubes[15];
    mid_side.c6 = &Cubes[25];
    mid_side.c7 = &Cubes[8];
    mid_side.c8 = &Cubes[18];
    mid_side.c9 = &Cubes[28];
    mid_side.others[0] = &Cubes[21]; mid_side.others[1] = &Cubes[11]; mid_side.others[2] = &Cubes[1];
    mid_side.others[3] = &Cubes[23]; mid_side.others[4] = &Cubes[13]; mid_side.others[5] = &Cubes[3];
    mid_side.others[6] = &Cubes[24]; mid_side.others[7] = &Cubes[14]; mid_side.others[8] = &Cubes[4];
    mid_side.others[9] = &Cubes[26]; mid_side.others[10] = &Cubes[16]; mid_side.others[11] = &Cubes[6];
    mid_side.others[12] = &Cubes[27]; mid_side.others[13] = &Cubes[17]; mid_side.others[14] = &Cubes[7];
    mid_side.others[15] = &Cubes[29]; mid_side.others[16] = &Cubes[19]; mid_side.others[17] = &Cubes[9];
    mid_side.face_faces = NULL;
    ring_p = &mid_side.ring_faces;
    *ring_p++ = &Up_Faces[7]; *ring_p++ = &Up_Faces[4]; *ring_p++ = &Up_Faces[1]; // up 2 12 22 
    *ring_p++ = &Back_Faces[1]; *ring_p++ = &Back_Faces[4]; *ring_p++ = &Back_Faces[7]; // back 22 25 28
    *ring_p++ = &Down_Faces[1];  *ring_p++ = &Down_Faces[4]; *ring_p++ = &Down_Faces[7]; // down 28 18 8
    *ring_p++ = &Front_Faces[7]; *ring_p++ = &Front_Faces[4]; *ring_p++ = &Front_Faces[1]; // front 8 5 2
    
    right.c1 = &Cubes[3];
    right.c2 = &Cubes[13];
    right.c3 = &Cubes[23];
    right.c4 = &Cubes[6];
    right.c5 = &Cubes[16];
    right.c6 = &Cubes[26];
    right.c7 = &Cubes[9];
    right.c8 = &Cubes[19];
    right.c9 = &Cubes[29];
    right.others[0] = &Cubes[22]; right.others[1] = &Cubes[12]; right.others[2] = &Cubes[2];
    right.others[3] = &Cubes[21]; right.others[4] = &Cubes[11]; right.others[5] = &Cubes[1];
    right.others[6] = &Cubes[25]; right.others[7] = &Cubes[15]; right.others[8] = &Cubes[5];
    right.others[9] = &Cubes[24]; right.others[10] = &Cubes[14]; right.others[11] = &Cubes[4];
    right.others[12] = &Cubes[28]; right.others[13] = &Cubes[18]; right.others[14] = &Cubes[8];
    right.others[15] = &Cubes[27]; right.others[16] = &Cubes[17]; right.others[17] = &Cubes[7];
    right.face_faces = &Right_Faces;
    ring_p = &right.ring_faces;
    *ring_p++ = &Up_Faces[8]; *ring_p++ = &Up_Faces[5]; *ring_p++ = &Up_Faces[2]; // up 3 13 23
    *ring_p++ = &Back_Faces[2]; *ring_p++ = &Back_Faces[5]; *ring_p++ = &Back_Faces[8]; // back 23 26 29
    *ring_p++ = &Down_Faces[2]; *ring_p++ = &Down_Faces[5]; *ring_p++ = &Down_Faces[8]; // down 29 19 9
    *ring_p++ = &Front_Faces[8]; *ring_p++ = &Front_Faces[5]; *ring_p++ = &Front_Faces[2]; // front 9 6 3
    
    top.c1 = &Cubes[21];
    top.c2 = &Cubes[22];
    top.c3 = &Cubes[23];
    top.c4 = &Cubes[11];
    top.c5 = &Cubes[12];
    top.c6 = &Cubes[13];
    top.c7 = &Cubes[1];
    top.c8 = &Cubes[2];
    top.c9 = &Cubes[3];
    top.others[0] = &Cubes[24]; top.others[1] = &Cubes[25]; top.others[2] = &Cubes[26];
    top.others[3] = &Cubes[27]; top.others[4] = &Cubes[28]; top.others[5] = &Cubes[29];
    top.others[6] = &Cubes[14]; top.others[7] = &Cubes[15]; top.others[8] = &Cubes[16];
    top.others[9] = &Cubes[17]; top.others[10] = &Cubes[18]; top.others[11] = &Cubes[19];
    top.others[12] = &Cubes[4]; top.others[13] = &Cubes[5]; top.others[14] = &Cubes[6];
    top.others[15] = &Cubes[7]; top.others[16] = &Cubes[8]; top.others[17] = &Cubes[9];
    top.face_faces = &Up_Faces;
    ring_p = &top.ring_faces;
    *ring_p++ = &Front_Faces[2]; *ring_p++ = &Front_Faces[1]; *ring_p++ = &Front_Faces[0]; // front 3 2 1
    *ring_p++ = &Left_Faces[0]; *ring_p++ = &Left_Faces[1]; *ring_p++ = &Left_Faces[2]; // left 1 11 21
    *ring_p++ = &Back_Faces[0]; *ring_p++ = &Back_Faces[1]; *ring_p++ = &Back_Faces[2]; // back 21 22 23
    *ring_p++ = &Right_Faces[2]; *ring_p++ = &Right_Faces[1]; *ring_p++ = &Right_Faces[0]; // right 23 13 3
    
    mid_h.c1 = &Cubes[24];
    mid_h.c2 = &Cubes[25];
    mid_h.c3 = &Cubes[26];
    mid_h.c4 = &Cubes[14];
    mid_h.c5 = &Cubes[15];
    mid_h.c6 = &Cubes[16];
    mid_h.c7 = &Cubes[4];
    mid_h.c8 = &Cubes[5];
    mid_h.c9 = &Cubes[6];
    mid_h.others[0] = &Cubes[21]; mid_h.others[1] = &Cubes[22]; mid_h.others[2] = &Cubes[23];
    mid_h.others[3] = &Cubes[27]; mid_h.others[4] = &Cubes[28]; mid_h.others[5] = &Cubes[29];
    mid_h.others[6] = &Cubes[11]; mid_h.others[7] = &Cubes[12]; mid_h.others[8] = &Cubes[13];
    mid_h.others[9] = &Cubes[17]; mid_h.others[10] = &Cubes[18]; mid_h.others[11] = &Cubes[19];
    mid_h.others[12] = &Cubes[1]; mid_h.others[13] = &Cubes[2]; mid_h.others[14] = &Cubes[3];
    mid_h.others[15] = &Cubes[7]; mid_h.others[16] = &Cubes[8]; mid_h.others[17] = &Cubes[9];
    mid_h.face_faces = NULL;
    ring_p = &mid_h.ring_faces;
    *ring_p++ = &Front_Faces[5]; *ring_p++ = &Front_Faces[4]; *ring_p++ = &Front_Faces[3]; // front 6 5 4
    *ring_p++ = &Left_Faces[3]; *ring_p++ = &Left_Faces[4]; *ring_p++ = &Left_Faces[5]; // left 4 14 24
    *ring_p++ = &Back_Faces[3]; *ring_p++ = &Back_Faces[4]; *ring_p++ = &Back_Faces[5]; // back 24 25 26
    *ring_p++ = &Right_Faces[5]; *ring_p++ = &Right_Faces[4]; *ring_p++ = &Right_Faces[3]; // right 26 16 6

    bottom.c1 = &Cubes[27];
    bottom.c2 = &Cubes[28];
    bottom.c3 = &Cubes[29];
    bottom.c4 = &Cubes[17];
    bottom.c5 = &Cubes[18];
    bottom.c6 = &Cubes[19];
    bottom.c7 = &Cubes[7];
    bottom.c8 = &Cubes[8];
    bottom.c9 = &Cubes[9];
    bottom.others[0] = &Cubes[21]; bottom.others[1] = &Cubes[22]; bottom.others[2] = &Cubes[23];
    bottom.others[3] = &Cubes[24]; bottom.others[4] = &Cubes[25]; bottom.others[5] = &Cubes[26];
    bottom.others[6] = &Cubes[11]; bottom.others[7] = &Cubes[12]; bottom.others[8] = &Cubes[13];
    bottom.others[9] = &Cubes[14]; bottom.others[10] = &Cubes[15]; bottom.others[11] = &Cubes[16];
    bottom.others[12] = &Cubes[1]; bottom.others[13] = &Cubes[2]; bottom.others[14] = &Cubes[3];
    bottom.others[15] = &Cubes[4]; bottom.others[16] = &Cubes[5]; bottom.others[17] = &Cubes[6];
    bottom.face_faces = &Down_Faces;
    ring_p = &bottom.ring_faces;
    *ring_p++ = &Front_Faces[8]; *ring_p++ = &Front_Faces[7]; *ring_p++ = &Front_Faces[6]; // front 9 8 7 
    *ring_p++ = &Left_Faces[6]; *ring_p++ = &Left_Faces[7]; *ring_p++ = &Left_Faces[8]; // left 7 17 27
    *ring_p++ = &Back_Faces[6]; *ring_p++ = &Back_Faces[7]; *ring_p++ = &Back_Faces[8]; // back 27 28 29
    *ring_p++ = &Right_Faces[8]; *ring_p++ = &Right_Faces[7]; *ring_p++ = &Right_Faces[6]; // right 29 19 9
}

void DrawCubeTexture_6(Cube cube, Vector4 rotation)
{ // Draw textured cube
    float x = cube.position.x;
    float y = cube.position.y;
    float z = cube.position.z;
    float width = 2.0f;
    float height = 2.0f;
    float length = 2.0f;
    Color color = WHITE;
       
    rlPushMatrix();
        rlTranslatef(-x, -y, -z);
        rlMultMatrixf(&cube.matrix);
        Vector3 rotated = Vector3Transform(cube.position, cube.matrix);
        rlTranslatef(rotated.x, rotated.y, rotated.z);

        if (rotation.w != 0) {
            Matrix rot = MatrixRotate((Vector3) { rotation.x, rotation.y, rotation.z }, rotation.w);
            rlMultMatrixf(&rot);
        }
        
    rlSetTexture(Textures[0].id);
    rlBegin(RL_QUADS);
    rlColor4ub(color.r, color.g, color.b, color.a);

    // Back Face
    rlNormal3f(0.0f, 0.0f, -1.0f);                  // Normal Pointing Away From Viewer
    rlTexCoord2f(1.0f, 0.0f); rlVertex3f(x - width / 2, y - height / 2, z - length / 2);  // Bottom Right Of The Texture and Quad
    rlTexCoord2f(1.0f, 1.0f); rlVertex3f(x - width / 2, y + height / 2, z - length / 2);  // Top Right Of The Texture and Quad
    rlTexCoord2f(0.0f, 1.0f); rlVertex3f(x + width / 2, y + height / 2, z - length / 2);  // Top Left Of The Texture and Quad
    rlTexCoord2f(0.0f, 0.0f); rlVertex3f(x + width / 2, y - height / 2, z - length / 2);  // Bottom Left Of The Texture and Quad
    
     // Left Face
    rlNormal3f(-1.0f, 0.0f, 0.0f);                  // Normal Pointing Left
    rlTexCoord2f(0.0f, 0.0f); rlVertex3f(x - width / 2, y - height / 2, z - length / 2);  // Bottom Left Of The Texture and Quad
    rlTexCoord2f(1.0f, 0.0f); rlVertex3f(x - width / 2, y - height / 2, z + length / 2);  // Bottom Right Of The Texture and Quad
    rlTexCoord2f(1.0f, 1.0f); rlVertex3f(x - width / 2, y + height / 2, z + length / 2);  // Top Right Of The Texture and Quad
    rlTexCoord2f(0.0f, 1.0f); rlVertex3f(x - width / 2, y + height / 2, z - length / 2);  // Top Left Of The Texture and Quad
    rlEnd();
 
    // Bottom Face
    rlNormal3f(0.0f, -1.0f, 0.0f);                  // Normal Pointing Down
    rlTexCoord2f(1.0f, 1.0f); rlVertex3f(x - width / 2, y - height / 2, z - length / 2);  // Top Right Of The Texture and Quad
    rlTexCoord2f(0.0f, 1.0f); rlVertex3f(x + width / 2, y - height / 2, z - length / 2);  // Top Left Of The Texture and Quad
    rlTexCoord2f(0.0f, 0.0f); rlVertex3f(x + width / 2, y - height / 2, z + length / 2);  // Bottom Left Of The Texture and Quad
    rlTexCoord2f(1.0f, 0.0f); rlVertex3f(x - width / 2, y - height / 2, z + length / 2);  // Bottom Right Of The Texture and Quad
    
    rlSetTexture(cube.right->id);

    // Right face
    rlNormal3f(1.0f, 0.0f, 0.0f);                  // Normal Pointing Right
    rlTexCoord2f(1.0f, 0.0f); rlVertex3f(x + width / 2, y - height / 2, z - length / 2);  // Bottom Right Of The Texture and Quad
    rlTexCoord2f(1.0f, 1.0f); rlVertex3f(x + width / 2, y + height / 2, z - length / 2);  // Top Right Of The Texture and Quad
    rlTexCoord2f(0.0f, 1.0f); rlVertex3f(x + width / 2, y + height / 2, z + length / 2);  // Top Left Of The Texture and Quad
    rlTexCoord2f(0.0f, 0.0f); rlVertex3f(x + width / 2, y - height / 2, z + length / 2);  // Bottom Left Of The Texture and Quad
    
    rlSetTexture(cube.top->id);

    // Top Face
    rlNormal3f(0.0f, 1.0f, 0.0f);                  // Normal Pointing Up
    rlTexCoord2f(0.0f, 1.0f); rlVertex3f(x - width / 2, y + height / 2, z - length / 2);  // Top Left Of The Texture and Quad
    rlTexCoord2f(0.0f, 0.0f); rlVertex3f(x - width / 2, y + height / 2, z + length / 2);  // Bottom Left Of The Texture and Quad
    rlTexCoord2f(1.0f, 0.0f); rlVertex3f(x + width / 2, y + height / 2, z + length / 2);  // Bottom Right Of The Texture and Quad
    rlTexCoord2f(1.0f, 1.0f); rlVertex3f(x + width / 2, y + height / 2, z - length / 2);  // Top Right Of The Texture and Quad

    rlSetTexture(cube.left->id);

    // Front Face
    rlNormal3f(0.0f, 0.0f, 1.0f);                  // Normal Pointing Towards Viewer
    rlTexCoord2f(0.0f, 0.0f); rlVertex3f(x - width / 2, y - height / 2, z + length / 2);  // Bottom Left Of The Texture and Quad
    rlTexCoord2f(1.0f, 0.0f); rlVertex3f(x + width / 2, y - height / 2, z + length / 2);  // Bottom Right Of The Texture and Quad
    rlTexCoord2f(1.0f, 1.0f); rlVertex3f(x + width / 2, y + height / 2, z + length / 2);  // Top Right Of The Texture and Quad
    rlTexCoord2f(0.0f, 1.0f); rlVertex3f(x - width / 2, y + height / 2, z + length / 2);  // Top Left Of The Texture and Quad
   
    rlPopMatrix();

    rlSetTexture(0);
}

void DrawAll()
{   // Draw cubes while there's no rotation
    for (int i = 1; i < 30; i++) {
        if (i % 10 != 0)
            DrawCubeTexture_6(Cubes[i], null);
    }
}

void Rotation(Side* side, Matrix* rot_matrix, int rotating) // CW watching from/through front/left side
{ // Apply all changes due to a rotation
    
    if (!Flags.undoing && !Flags.redoing) { // record moves in the move buffer
        if (min_redo == p_undo+1) {
            min_redo++;
            if (min_redo == (undo_buffer + UNDOBUFFER_SIZE))
                min_redo = undo_buffer;
        }
        *p_undo++ = rotating;
        if (p_undo == (undo_buffer + UNDOBUFFER_SIZE)) {
            p_undo = undo_buffer;
            if (min_redo == undo_buffer) min_redo++;
        }
        max_redo = p_undo;
    }
    Cube temp = *side->c1;
    Face temp_f;
    Matrix* checkmatrix;
    
    if (rot_matrix == &x_axis_CW) checkmatrix = &x_axis_CCW;
    else if (rot_matrix == &x_axis_CCW) checkmatrix = &x_axis_CW;
    else checkmatrix = rot_matrix;

    side->c5->matrix = MatrixMultiply(*rot_matrix, side->c5->matrix);
    side->c5->check = Vector3Transform(side->c5->check, *checkmatrix);

    if (rot_matrix == &z_axis_CW || rot_matrix == &y_axis_CW || rot_matrix == &x_axis_CW) {
        side->c1->top = side->c7->top;
        side->c1->left = side->c7->left;
        side->c1->right = side->c7->right;
        side->c1->matrix = MatrixMultiply(*rot_matrix, side->c7->matrix);
        side->c1->check = Vector3Transform(side->c7->check, *checkmatrix);
        side->c7->top = side->c9->top;
        side->c7->left = side->c9->left;
        side->c7->right = side->c9->right;
        side->c7->matrix = MatrixMultiply(*rot_matrix, side->c9->matrix);
        side->c7->check = Vector3Transform(side->c9->check, *checkmatrix);
        side->c9->top = side->c3->top;
        side->c9->left = side->c3->left;
        side->c9->right = side->c3->right;
        side->c9->matrix = MatrixMultiply(*rot_matrix, side->c3->matrix);
        side->c9->check = Vector3Transform(side->c3->check, *checkmatrix);
        side->c3->top = temp.top;
        side->c3->left = temp.left;
        side->c3->right = temp.right;
        side->c3->matrix = MatrixMultiply(*rot_matrix, temp.matrix);
        side->c3->check = Vector3Transform(temp.check, *checkmatrix);

        temp = *side->c2;
        side->c2->top = side->c4->top;
        side->c2->left = side->c4->left;
        side->c2->right = side->c4->right;
        side->c2->matrix = MatrixMultiply(*rot_matrix, side->c4->matrix);
        side->c2->check = Vector3Transform(side->c4->check, *checkmatrix);
        side->c4->top = side->c8->top;
        side->c4->left = side->c8->left;
        side->c4->right = side->c8->right;
        side->c4->matrix = MatrixMultiply(*rot_matrix, side->c8->matrix);
        side->c4->check = Vector3Transform(side->c8->check, *checkmatrix);
        side->c8->top = side->c6->top;
        side->c8->left = side->c6->left;
        side->c8->right = side->c6->right;
        side->c8->matrix = MatrixMultiply(*rot_matrix, side->c6->matrix);
        side->c8->check = Vector3Transform(side->c6->check, *checkmatrix);
        side->c6->top = temp.top;
        side->c6->left = temp.left;
        side->c6->right = temp.right;
        side->c6->matrix = MatrixMultiply(*rot_matrix, temp.matrix);
        side->c6->check = Vector3Transform(temp.check, *checkmatrix);
        
        if (side->face_faces != NULL) {
            temp_f = side->face_faces[0];
            side->face_faces[0].check = side->face_faces[6].check;
            side->face_faces[6].check = side->face_faces[8].check;
            side->face_faces[8].check = side->face_faces[2].check;
            side->face_faces[2].check = temp_f.check;
            temp_f = side->face_faces[1];
            side->face_faces[1].check = side->face_faces[3].check;
            side->face_faces[3].check = side->face_faces[7].check;
            side->face_faces[7].check = side->face_faces[5].check;
            side->face_faces[5].check = temp_f.check;
        }
        for (int i = 0; i < 3; i++) {
            temp_f = *side->ring_faces[i];
            (*side->ring_faces[i]).check = (*side->ring_faces[i + 9]).check;
            (*side->ring_faces[i + 9]).check = (*side->ring_faces[i + 6]).check;
            (*side->ring_faces[i + 6]).check = (*side->ring_faces[i + 3]).check;
            (*side->ring_faces[i + 3]).check = temp_f.check;
        }
    }
    else {
        side->c1->top = side->c3->top;
        side->c1->left = side->c3->left;
        side->c1->right = side->c3->right;
        side->c1->matrix = MatrixMultiply(*rot_matrix, side->c3->matrix);
        side->c1->check = Vector3Transform(side->c3->check, *checkmatrix);
        side->c3->top = side->c9->top;
        side->c3->left = side->c9->left;
        side->c3->right = side->c9->right;
        side->c3->matrix = MatrixMultiply(*rot_matrix, side->c9->matrix);
        side->c3->check = Vector3Transform(side->c9->check, *checkmatrix);
        side->c9->top = side->c7->top;
        side->c9->left = side->c7->left;
        side->c9->right = side->c7->right;
        side->c9->matrix = MatrixMultiply(*rot_matrix, side->c7->matrix);
        side->c9->check = Vector3Transform(side->c7->check, *checkmatrix);
        side->c7->top = temp.top;
        side->c7->left = temp.left;
        side->c7->right = temp.right;
        side->c7->matrix = MatrixMultiply(*rot_matrix, temp.matrix);
        side->c7->check = Vector3Transform(temp.check, *checkmatrix);

        temp = *side->c2;
        side->c2->top = side->c6->top;
        side->c2->left = side->c6->left;
        side->c2->right = side->c6->right;
        side->c2->matrix = MatrixMultiply(*rot_matrix, side->c6->matrix);
        side->c2->check = Vector3Transform(side->c6->check, *checkmatrix);
        side->c6->top = side->c8->top;
        side->c6->left = side->c8->left;
        side->c6->right = side->c8->right;
        side->c6->matrix = MatrixMultiply(*rot_matrix, side->c8->matrix);
        side->c6->check = Vector3Transform(side->c8->check, *checkmatrix);
        side->c8->top = side->c4->top;
        side->c8->left = side->c4->left;
        side->c8->right = side->c4->right;
        side->c8->matrix = MatrixMultiply(*rot_matrix, side->c4->matrix);
        side->c8->check = Vector3Transform(side->c4->check, *checkmatrix);
        side->c4->top = temp.top;
        side->c4->left = temp.left;
        side->c4->right = temp.right;
        side->c4->matrix = MatrixMultiply(*rot_matrix, temp.matrix);
        side->c4->check = Vector3Transform(temp.check, *checkmatrix);
        
        if (side->face_faces != NULL) {
            temp_f = side->face_faces[0];
            side->face_faces[0].check = side->face_faces[2].check;
            side->face_faces[2].check = side->face_faces[8].check;
            side->face_faces[8].check = side->face_faces[6].check;
            side->face_faces[6].check = temp_f.check;
            temp_f = side->face_faces[1];
            side->face_faces[1].check = side->face_faces[5].check;
            side->face_faces[5].check = side->face_faces[7].check;
            side->face_faces[7].check = side->face_faces[3].check;
            side->face_faces[3].check = temp_f.check;
        }
        for (int i = 0; i < 3; i++) {
            temp_f = *side->ring_faces[i];
            (*side->ring_faces[i]).check = (*side->ring_faces[i + 3]).check;
            (*side->ring_faces[i + 3]).check = (*side->ring_faces[i + 6]).check;
            (*side->ring_faces[i + 6]).check = (*side->ring_faces[i + 9]).check;
            (*side->ring_faces[i + 9]).check = temp_f.check;
        }
    }
}

void InitLookup() 
{ // Lookup table for rotations
    rotation_table[0].side = &front;
    rotation_table[0].rot = (Vector4) {0, 0, 1, 0};
    rotation_table[0].rot_matrix = &z_axis_CW;
    rotation_table[1].side = &mid_front;
    rotation_table[1].rot = (Vector4){ 0, 0, 1, 0 };
    rotation_table[1].rot_matrix = &z_axis_CW;
    rotation_table[2].side = &back;
    rotation_table[2].rot = (Vector4){ 0, 0, 1, 0 };
    rotation_table[2].rot_matrix = &z_axis_CW;
    rotation_table[3].side = &top;
    rotation_table[3].rot = (Vector4){ 0, 1, 0, 0 };
    rotation_table[3].rot_matrix = &y_axis_CW;
    rotation_table[4].side = &mid_h;
    rotation_table[4].rot = (Vector4){ 0, 1, 0, 0 };
    rotation_table[4].rot_matrix = &y_axis_CW;
    rotation_table[5].side = &bottom;
    rotation_table[5].rot = (Vector4){ 0, 1, 0, 0 };
    rotation_table[5].rot_matrix = &y_axis_CW;
    rotation_table[6].side = &left;
    rotation_table[6].rot = (Vector4){ 1, 0, 0, 0 };
    rotation_table[6].rot_matrix = &x_axis_CW;
    rotation_table[7].side = &mid_side;
    rotation_table[7].rot = (Vector4){ 1, 0, 0, 0 };
    rotation_table[7].rot_matrix = &x_axis_CW;
    rotation_table[8].side = &right;
    rotation_table[8].rot = (Vector4){ 1, 0, 0, 0 };
    rotation_table[8].rot_matrix = &x_axis_CW;
    rotation_table[9].side = &front;
    rotation_table[9].rot = (Vector4){ 0, 0, -1, 0 };
    rotation_table[9].rot_matrix = &z_axis_CCW;
    rotation_table[10].side = &mid_front;
    rotation_table[10].rot = (Vector4){ 0, 0, -1, 0 };
    rotation_table[10].rot_matrix = &z_axis_CCW;
    rotation_table[11].side = &back;
    rotation_table[11].rot = (Vector4){ 0, 0, -1, 0 };
    rotation_table[11].rot_matrix = &z_axis_CCW;
    rotation_table[12].side = &top;
    rotation_table[12].rot = (Vector4){ 0, -1, 0, 0 };
    rotation_table[12].rot_matrix = &y_axis_CCW;
    rotation_table[13].side = &mid_h;
    rotation_table[13].rot = (Vector4){ 0, -1, 0, 0 };
    rotation_table[13].rot_matrix = &y_axis_CCW;
    rotation_table[14].side = &bottom;
    rotation_table[14].rot = (Vector4){ 0, -1, 0, 0 };
    rotation_table[14].rot_matrix = &y_axis_CCW;
    rotation_table[15].side = &left;
    rotation_table[15].rot = (Vector4){ -1, 0, 0, 0 };
    rotation_table[15].rot_matrix = &x_axis_CCW;
    rotation_table[16].side = &mid_side;
    rotation_table[16].rot = (Vector4){ -1, 0, 0, 0 };
    rotation_table[16].rot_matrix = &x_axis_CCW;
    rotation_table[17].side = &right;
    rotation_table[17].rot = (Vector4){ -1, 0, 0, 0 };
    rotation_table[17].rot_matrix = &x_axis_CCW;
}

void InitAll()
{
    InitRotationMatrices();
    InitTextures();
    InitCubes();
    InitSides();
    InitFaces();
    InitLookup();
}

void DrawRotation(Side* side, Vector4 rotation)
{ // Draw all cubes while a side is rotating
    Cube** C = &(side->c1);
    for (int i = 0; i < 9; i++) {
        DrawCubeTexture_6(**C++, rotation);
    }

    for (int i = 0; i < 18; i++) {
        DrawCubeTexture_6(*side->others[i], null);
    }
}

void Undo()
{
    if (p_undo != min_redo) {
        int i = 0;
        Flags.undoing = 1;
        i = *--p_undo;
        if (p_undo < undo_buffer) {
            p_undo += UNDOBUFFER_SIZE;
            i = *p_undo;
        }
        i = (i > 8) ? (i - 9) : (i + 9);
        rotating = i;
        rotating_side = rotation_table[rotating].side;
        rotation = rotation_table[rotating].rot;
        rot_multiplier = 4;
        move_counter--;
    }
}

void Redo()
{
    if (p_undo != max_redo) {
        Flags.redoing = 1;
        rotating = *p_undo++;
        if (p_undo == (undo_buffer + UNDOBUFFER_SIZE)) {
            p_undo = undo_buffer;
        }
        rotating_side = rotation_table[rotating].side;
        rotation = rotation_table[rotating].rot;
        rot_multiplier = 4;
        move_counter++;
    }
}

void UpdateCustomCamera(Camera* camera)
{
    // Mouse movement detection
    Vector2 mousePositionDelta = { 0.0f, 0.0f };
    Vector2 mousePosition = GetMousePosition();

    bool keyrotate = IsMouseButtonDown(C_CAMERA.rotationControl);

    mousePositionDelta.x = mousePosition.x - C_CAMERA.previousMousePosition.x;
    mousePositionDelta.y = mousePosition.y - C_CAMERA.previousMousePosition.y;

    C_CAMERA.previousMousePosition = mousePosition;

    if (keyrotate)
    {
        C_CAMERA.angle.x += mousePositionDelta.x * -CAMERA_FREE_MOUSE_SENSITIVITY * camera->up.y;
        C_CAMERA.angle.y += mousePositionDelta.y * -CAMERA_FREE_MOUSE_SENSITIVITY;
    }
    // Wrap around angle values
    
    while (C_CAMERA.angle.y > M_PI) {
        C_CAMERA.angle.y -= 2 * M_PI;
    }
    while (C_CAMERA.angle.y < -M_PI) {
        C_CAMERA.angle.y += 2 * M_PI;
    }
    while (C_CAMERA.angle.x > M_PI) {
        C_CAMERA.angle.x -= 2 * M_PI;
    }
    while (C_CAMERA.angle.x < -M_PI) {
        C_CAMERA.angle.x += 2 * M_PI;
    }
    // Update camera position with changes
    camera->position.x = -sinf(C_CAMERA.angle.x) * C_CAMERA.targetDistance * cosf(C_CAMERA.angle.y) + camera->target.x;
    camera->position.y = -sinf(C_CAMERA.angle.y) * C_CAMERA.targetDistance + camera->target.y;
    camera->position.z = -cosf(C_CAMERA.angle.x) * C_CAMERA.targetDistance * cosf(C_CAMERA.angle.y) + camera->target.z;
    // Rotate camera along its axis if needed
    if ((C_CAMERA.angle.y >= M_PI_2) 
        || (C_CAMERA.angle.y <= -M_PI_2)) {
        camera->up.y = -1;
    }
    else {
        camera->up.y = 1;
    }
}

void SetCustomCameraMode(Camera camera)
{
    Vector3 v1 = camera.position;
    Vector3 v2 = camera.target;

    float dx = v2.x - v1.x;
    float dy = v2.y - v1.y;
    float dz = v2.z - v1.z;

    C_CAMERA.targetDistance = sqrtf(dx * dx + dy * dy + dz * dz);   

    // Camera angle calculation
    C_CAMERA.angle.x = atan2f(dx, dz);                        // Camera angle in plane XZ (0 aligned with Z, move positive CCW)
    C_CAMERA.angle.y = atan2f(dy, sqrtf(dx * dx + dz * dz));      // Camera angle in plane XY (0 aligned with X, move positive CW)

    C_CAMERA.previousMousePosition = GetMousePosition();      // Init mouse position
}

void DrawTime(double time, int x, int y)
{
    int h = 0; // hours
    int m = 0; // minutes
    
    while (time > 3600) {
        h++;
        time -= 3600.0;
    }
    while (time > 60) {
        m++;
        time -= 60.0;
    }
    
    char hours[5];
    char minutes[5];
    char seconds[20];
    char print_string[25] = { '\0' };
    char base[] = "Time ";
    sprintf(hours, "%d", h);
    sprintf(minutes, "%d", m);
    
    sprintf(seconds, "%.03f", time);
    strcat(print_string, base); 

    if (h) {
        strcat(print_string, hours);
        strcat(print_string, "h");
        if (!m) {
            strcat(print_string, "0m");
        }
    }
    if (m) {
        strcat(print_string, minutes);
        strcat(print_string, "m");
    }
    strcat(print_string, seconds); strcat(print_string, "s");
    DrawText(print_string, x, y, TEXT_SIZE, BLACK);
}

void DrawDebug(Camera camera, CustomCameraData C_CAMERA) // Useful debug info
{ // Useful debug info
    char mx[20];
    sprintf(mx, "%d", GetMouseX());
    DrawText("Mouse X", 10, 10, 20, GRAY);
    DrawText(mx, 100, 10, 20, GRAY);
    char my[20];
    sprintf(my, "%d", GetMouseY());
    DrawText("Mouse Y", 10, 30, 20, GRAY);
    DrawText(my, 100, 30, 20, GRAY);
    char x[20];
    sprintf(x, "%f", camera.position.x);
    DrawText("pos X", 10, 50, 20, GRAY);
    DrawText(x, 70, 50, 20, GRAY);
    char y[20];
    sprintf(y, "%f", camera.position.y);
    DrawText("pos Y", 10, 70, 20, GRAY);
    DrawText(y, 70, 70, 20, GRAY);
    char z[20];
    sprintf(z, "%f", camera.position.z);
    DrawText("pos Z", 10, 90, 20, GRAY);
    DrawText(z, 70, 90, 20, GRAY);
    char cx[20];
    sprintf(cx, "%f", C_CAMERA.angle.x);
    DrawText("ang X", 10, 110, 20, GRAY);
    DrawText(cx, 70, 110, 20, GRAY);
    char cy[20];
    sprintf(cy, "%f", C_CAMERA.angle.y);
    DrawText("ang Y", 10, 130, 20, GRAY);
    DrawText(cy, 70, 130, 20, GRAY);
    char cz[20];
    sprintf(cz, "%f", camera.up.y);
    DrawText("ang Y", 10, 150, 20, GRAY);
    DrawText(cz, 70, 170, 20, GRAY);
    
    for (int i = 0; i < 54; i++) {
        DrawLine3D(Faces[i]->center, Vector3Add(Faces[i]->center, Faces[i]->check), BLACK);
    }
    
    for (int i = 1; i < 30; i++) {
        if (i % 10 != 0)
            DrawLine3D(Cubes[i].position, Vector3Add(Cubes[i].position, Cubes[i].check), BLACK);
    }
}

void Menu()
{
    static int up = (ScreenHeight / 2 - 2 * ScreenHeight / 25);
    static int down = (ScreenHeight / 2 + ScreenHeight / 25);
    static int arrow_pos = 0;
    if (arrow_pos == 0) arrow_pos = up;

    if (GetMouseY() > ScreenHeight / 2)
        arrow_pos = down;
    else
        arrow_pos = up;
    if (IsKeyDown(KEY_ENTER) || IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            Flags.textured = (arrow_pos == down) ? 1 : 0;
            move_counter = Flags.was_solved = Flags.started = 0;
            InitAll();
            while (++scrambling < 30) {
                int setup = rand() % 18;
                rotating_side = rotation_table[setup].side;
                rotation = rotation_table[setup].rot;
                Rotation(rotating_side, rotation_table[setup].rot_matrix, rotating);
            }
            scrambling = 0;
            Flags.setup = 0;
            p_undo = max_redo = min_redo = undo_buffer;
        }
    DrawText("Standard Cube", ScreenWidth / 3, ScreenHeight / 2 - 2 * ScreenHeight / 25, TEXT_SIZE_MENU, BLACK);
    DrawText("Zachtronics Cube", ScreenWidth / 3, ScreenHeight / 2 + ScreenHeight / 25, TEXT_SIZE_MENU, BLACK);
    DrawText("->", ScreenWidth / 3 - ScreenHeight / 8, arrow_pos, TEXT_SIZE_MENU, BLACK);
}

void CheckIfSolved()
{
    float epsilon = 0.0001f;
    Flags.solved = 1;
    if (Flags.textured) {
        Vector3 v = Cubes[1].check;
        for (int i = 2; i < 30; i++) {
            if (i % 10 == 0) continue;                
            if ((fabs((Cubes[i].check.x - v.x)) > epsilon) || (fabs((Cubes[i].check.y - v.y)) > epsilon) || (fabs((Cubes[i].check.z - v.z)) > epsilon)) {
                Flags.solved = 0;
                break;
            }
        }
    }
    else {
        for (int j = 0; j < 6; j++) {
            if (!Flags.solved) 
                break;
            for (int i = 1; i < 9; i++) {
                Vector3 v = (Faces[(9 * j) + i])->check;
                Vector3 w = (Faces[(9 * j) + i - 1])->check;
                if ((fabs(v.x - w.x) > epsilon) || (fabs(v.y - w.y) > epsilon) || (fabs(v.z - w.z) > epsilon)) {
                    Flags.solved = 0;
                    break;
                }
            }
        }
    }
}

void DrawInfo()
{
    if (Flags.started)
        ongoing_time = GetTime() - start_time;

    if (Flags.solved || Flags.was_solved) {
        Flags.was_solved = 1;
        if (end_time < 0.1)
            end_time = ongoing_time;
        if (Flags.solved)
            DrawText("Solved!", ScreenWidth / 2 - 30, ScreenHeight - (ScreenHeight / 20), TEXT_SIZE, BLACK);
        DrawTime(end_time, ScreenWidth * 3 / 4, ScreenHeight - (ScreenHeight / 20));
    }
    else if (Flags.started) {
        DrawTime(ongoing_time, ScreenWidth * 3 / 4, ScreenHeight - (ScreenHeight / 20));
    }
    char c[12];
    sprintf(c, "%d", move_counter);
    if (move_counter == 1) {
        strcat(c, " Move");
    }
    else {
        strcat(c, " Moves");
    }
    DrawText(c, 10, ScreenHeight - (ScreenHeight / 20), TEXT_SIZE, BLACK);
}

float GetOrthogonalLength(int len, Vector2 base, Vector2 movement)
{ // Get the length of a vector's orthogonal projection
    float result;
    float angle_n1 = Vector2Angle(movement, base);
    angle_n1 = (angle_n1 < 0) ? -angle_n1 : angle_n1;
    result = len * cos(angle_n1);
    return result;
}

void GimbalUnlock(float x)
{
    if (camera.position.y > 0) {
        if (x < -1 && x > -2) {
            Rotation(&front, &z_axis_CW, 0);
            Rotation(&mid_front, &z_axis_CW, 0);
            Rotation(&back, &z_axis_CW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 0, 0, 1 }, -M_PI_2);
        }
        else if (x > 1 && x < 2) {
            Rotation(&front, &z_axis_CCW, 0);
            Rotation(&mid_front, &z_axis_CCW, 0);
            Rotation(&back, &z_axis_CCW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 0, 0, 1 }, M_PI_2);
        }
        else if (x > 2.5 || x < -2.5) {
            Rotation(&left, &x_axis_CCW, 0);
            Rotation(&mid_side, &x_axis_CCW, 0);
            Rotation(&right, &x_axis_CCW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 1, 0, 0 }, M_PI_2);
        }
        else {
            Rotation(&left, &x_axis_CW, 0);
            Rotation(&mid_side, &x_axis_CW, 0);
            Rotation(&right, &x_axis_CW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 1, 0, 0 }, -M_PI_2);
        }
    }
    else {
        if (x < -1 && x > -2) {
            Rotation(&front, &z_axis_CCW, 0);
            Rotation(&mid_front, &z_axis_CCW, 0);
            Rotation(&back, &z_axis_CCW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 0, 0, 1 }, M_PI_2);
        }
        else if (x > 1 && x < 2) {
            Rotation(&front, &z_axis_CW, 0);
            Rotation(&mid_front, &z_axis_CW, 0);
            Rotation(&back, &z_axis_CW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 0, 0, 1 }, -M_PI_2);
        }
        else if (x > 2.5 || x < -2.5) {
            Rotation(&left, &x_axis_CW, 0);
            Rotation(&mid_side, &x_axis_CW, 0);
            Rotation(&right, &x_axis_CW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 1, 0, 0 }, -M_PI_2);
        }
        else {
            Rotation(&left, &x_axis_CCW, 0);
            Rotation(&mid_side, &x_axis_CCW, 0);
            Rotation(&right, &x_axis_CCW, 0);
            camera.position = Vector3RotateByAxisAngle(camera.position, (Vector3) { 1, 0, 0 }, M_PI_2);
        }
    }
}

void ProcessInputs()
{
    static int click_x;
    static int click_y;

    if (scrambling) {
        rotating = rand() % 18;
        rotating_side = rotation_table[rotating].side;
        rotation = rotation_table[rotating].rot;
        if (++scrambling == 30) {
            scrambling = 0;
            rotating = -1;
            end_time = 0.0f;
            move_counter = Flags.was_solved = Flags.started = 0;
            p_undo = max_redo = min_redo = undo_buffer;
        }
    }
    else if (CoolDown >= (1 / GetFrameTime()) / 12) {
        if ((IsKeyDown('S')) || (IsKeyDown('F')) || (IsKeyDown(KEY_BACKSPACE)) || (IsKeyDown(KEY_ENTER)))
            CoolDown = 0;

        if (IsKeyDown('S')) { // Scramble
            scrambling = 1;
            rot_multiplier = 4;
        }

        else if (IsKeyPressed('F')) { // Get out of gimbal lock
            Flags.redoing = 1; // set redo flag so the moves don't get added to the buffer
            GimbalUnlock(C_CAMERA.angle.x);
            Flags.redoing = 0;
            SetCustomCameraMode(camera);
        }

        else if (IsKeyDown(KEY_BACKSPACE) || GetMouseWheelMove() < 0) {
            Undo();
        }
        else if (IsKeyDown(KEY_ENTER) || GetMouseWheelMove() > 0) {
            Redo();
        }
        
        else if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && rotating < 0) {
            // Find which square was clicked
            Flags.successful_click = 0;
            p_faces = &Faces;
            float distance = 100000;
            click_x = GetMouseX();
            click_y = GetMouseY();
            raycast = GetMouseRay(GetMousePosition(), camera);
            for (int i = 0; i < 54; i++) {
                RayCollision test = GetRayCollisionQuad(raycast, (*p_faces)->a, (*p_faces)->b, (*p_faces)->c, (*p_faces)->d);
                if (test.hit) {
                    if (test.distance < distance) {
                        Flags.successful_click = 1;
                        rotating_face = *p_faces;
                        distance = test.distance;
                    }
                }
                p_faces++;
            }
        }
        else if (IsMouseButtonDown(MOUSE_BUTTON_LEFT) && Flags.successful_click) {
            // Process mouse movement to define rotation
            int x = GetMouseX();
            int y = GetMouseY();
            if (abs(x - click_x) > 10 || abs(y - click_y) > 10) {
                Flags.successful_click = 0;
                move_counter++;
                Vector2 a = GetWorldToScreen(rotating_face->neighbour1->center, camera);
                Vector2 b = GetWorldToScreen(rotating_face->neighbour2->center, camera);
                Vector2 c = GetWorldToScreen(rotating_face->center, camera);
                Vector2 n1 = { a.x - c.x, a.y - c.y };
                Vector2 rev_n1 = { -n1.x, -n1.y };
                Vector2 n2 = { b.x - c.x, b.y - c.y };
                Vector2 rev_n2 = { -n2.x, -n2.y };
                Vector2 movement = { click_x - x, click_y - y };

                // Take orthogonal projection of mouse movement on the vectors linking
                // a face to its neighbours. Max distance wins.
                int dmovement = Vector2Length(movement);

                float scalar_n1 = GetOrthogonalLength(dmovement, n1, movement);
                float scalar_rev_n1 = GetOrthogonalLength(dmovement, rev_n1, movement);
                float scalar_n2 = GetOrthogonalLength(dmovement, n2, movement);
                float scalar_rev_n2 = GetOrthogonalLength(dmovement, rev_n2, movement);

                if (scalar_n1 > scalar_rev_n1 && scalar_n1 > scalar_n2 && scalar_n1 > scalar_rev_n2) {
                    rotating = rotating_face->rotation1;
                    rotating = (rotating > 8) ? rotating - 9 : rotating + 9;
                }
                else if (scalar_rev_n1 > scalar_n1 && scalar_rev_n1 > scalar_n2 && scalar_rev_n1 > scalar_rev_n2)
                    rotating = rotating_face->rotation1;
                else if (scalar_n2 > scalar_n1 && scalar_n2 > scalar_rev_n1 && scalar_n2 > scalar_rev_n2) {
                    rotating = rotating_face->rotation2;
                    rotating = (rotating > 8) ? rotating - 9 : rotating + 9;
                }
                else {
                    rotating = rotating_face->rotation2;
                }
                rotating_side = rotation_table[rotating].side;
                rotation = rotation_table[rotating].rot;
                rot_multiplier = 1;
            }
        }
        else if (IsKeyDown('O')) { // Go back to camera starting point
            camera.position = (Vector3){ 10.0f, 10.0f, 10.0f };
            camera.up = (Vector3){ 0.0f, 1.0f, 0.0f };
            SetCustomCameraMode(camera);
        }
#if defined(PLATFORM_WEB)
        if (IsKeyDown(KEY_ESCAPE))
            Flags.setup = 1;
#endif
    }
}

void UpdateDrawFrame()
{  
    UpdateCustomCamera(&camera);
    CoolDown++;

    BeginDrawing();

    if (Flags.textured)
        ClearBackground((Color) { 230, 230, 230, 255 });
    else
        ClearBackground(RAYWHITE);

    if (Flags.setup) {
        Menu();
    }
    else {
        BeginMode3D(camera);

        if (rotating > -1) {
            if (!Flags.started && !scrambling) {
                Flags.started = 1;
                start_time = GetTime();
            }
            DrawRotation(rotating_side, rotation);

            rotation.w += M_PI_2 * GetFrameTime() * 8 * rot_multiplier;
            if (rotation.w >= M_PI_2) {
                Rotation(rotating_side, rotation_table[rotating].rot_matrix, rotating);
                rotating = -1;
                Flags.undoing = Flags.redoing = 0;
            }
        }
        else {
            DrawAll();
            ProcessInputs();
        }
        EndMode3D();
        CheckIfSolved();
        DrawInfo();
        //DrawDebug(camera, C_CAMERA);
    }
    EndDrawing();
}

int main(void)
{
    InitWindow(ScreenWidth, ScreenHeight, "Rubik's");
    Flags.setup = 1;
    camera.position = (Vector3){ 10.0f, 10.0f, 10.0f }; 
    camera.target = (Vector3){ 0.0f, 0.0f, 0.0f };      
    camera.up = (Vector3){ 0.0f, 1.0f, 0.0f };         // Camera up vector (rotation towards target)
    camera.fovy = 45.0f;                                // Camera field-of-view Y
    camera.projection = CAMERA_PERSPECTIVE;                   

    SetCustomCameraMode(camera); 
    
    SetRandomSeed(GetRandomValue(-12039, 2394834));

    SetTargetFPS(120);
    // The game loop is in a separate function for better performance in web version
#if defined(PLATFORM_WEB) 
    emscripten_set_main_loop(UpdateDrawFrame, 0, 1);
#else
    
    // Main game loop
    while (!WindowShouldClose())        // Detect window close button or ESC key
    {
        UpdateDrawFrame();
    }
#endif
    
    CloseWindow();        

    return 0;
}
